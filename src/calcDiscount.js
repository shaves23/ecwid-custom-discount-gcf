exports.calcDiscount = (req, res) => {
    res.status(200)
    let responseMsg = {};
    responseMsg.status = "-";

    discountObj = {};    
    discountObj.value = 1;
    discountObj.type = "PERCENT";
    discountObj.description = "Default : No surcharge applied";
    discountObj.appliesToProducts = {};       
    
    responseMsg.discounts = [];
    responseMsg.discounts.push(discountObj);

    try {
        try {
            if (req.body.cart.paymentMethod == null || req.body.cart.paymentMethod == '') {                
                responseMsg.status = "Empty or null paymentMethod";
                res.send(responseMsg);
                return;
            }
        } catch (error) {            
            responseMsg.status = "cart.paymentMethod parse Error : " + error;             
            res.send(responseMsg);
            return;
        }

        try {
            if ( req.body.cart.subtotal == null || req.body.cart.subtotal == '' ) {   
                responseMsg.status = "Empty or null subtotal";       
                res.send(responseMsg);
                return;
            }
        } catch (error) {
            responseMsg.status = "cart.subtotal parse Error : " + error;            
            res.send(responseMsg);
            return;
        }
    
        let paymentMethod = req.body.cart.paymentMethod;
        let subtotal = Number.parseFloat(req.body.cart.subtotal);

        if (subtotal > 0) {

            if (paymentMethod == "PayPal") {               
                discountObj.value = 5;                
                discountObj.description = "+5% Surcharge for PayPal payments";
                responseMsg.status = "PayPal surcharge applied";
            }
            else if (paymentMethod == "Stripe") {                
                discountObj.value = 15;                
                discountObj.description = "+15% Surcharge for Stripe payments";
                responseMsg.status = "Stripe surcharge applied";
            }
            else {
                discountObj.value = 50;                
                discountObj.description = "TEST 50% Surcharge applied, will be removed in Production";
                responseMsg.status = "No surcharge applied, unknown paymentMethod : " + paymentMethod;   
            } 
  
        }
        else            
            responseMsg.status = "No surcharge applied, subtotal is 0";    

    } catch (error) {
        responseMsg.status = "Error : " + error;    
    }    

    responseMsg.discounts = [];
    responseMsg.discounts.push(discountObj);

    res.send(responseMsg);


  };